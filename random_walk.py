import numpy as np
from racetrack import course
import sys

if __name__ == "__main__":
    print('-- random walk --')
    track = sys.argv[1]
    print(f'building from {track}')

    crs = course()
    crs.build_course(track)

    print(f'results of policy (average steps): {np.mean(crs.simulate(100))}')