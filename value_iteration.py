from random import choice, randint, random, uniform
import numpy as np
import logging
from racetrack import course
import sys

class ValueIterator():

    def __init__(self,gamma,error_magnitude):
        self.gamma = gamma
        self.error_magnitude = error_magnitude

    def initialize(self,course):
        self.course = course
        self.states = course.get_states()
        self.policy = {}
        for loc in self.states:
            self.policy[loc] = {}
            for spd in self.states[loc]:
                self.policy[loc][spd] = []

    def Q(self,state,action,restart=False):
        i_loc = state['loc']
        i_spd = state['spd']
        T_V = []
        for i,t in enumerate(state['tx']):
            pr = state['pr'][i]
            tx = [ action[0]*t, action[1]*t ]
            self.course.place_driver(i_loc,i_spd)
            f_loc,f_spd = self.course.project_driver(a_x=tx[0],a_y=tx[1],restart=restart)
            T_V.append(pr * self.states[str(f_loc)][str(f_spd)]['val'])
        return state['reward'] + self.gamma * sum(T_V)

    def update(self,restart=False):
        V = {}
        d_V = []
        for loc in self.states:
            V[loc] = {}
            for spd in self.states[loc]:
                V[loc][spd] = 0
                Q_s_a = []
                for action in self.states[loc][spd]['actions']:
                    Q_s_a.append(self.Q(self.states[loc][spd],action,restart))
                if len(Q_s_a) != 0:
                    Q_s_a = np.array(Q_s_a)
                    self.policy[loc][spd] = [ self.states[loc][spd]['actions'][i] for i in np.where(Q_s_a == Q_s_a.max())[0]]
                    Q_max = max(Q_s_a)
                    d_V.append(abs(Q_max - self.states[loc][spd]['val']))
                    V[loc][spd] = Q_max

        for loc in self.states:
            for spd in self.states[loc]:
                self.states[loc][spd]['val'] = V[loc][spd]

        result = max(d_V)

        self.course.driver.policy = self.policy

        logging.info(f'max(dV)={result}')

        return result

    def run(self,restart=False):
        logging.info('performing value iteration:')
        logging.info(f'gamma={self.gamma}')
        logging.info(f'Bellman error magnitude={self.error_magnitude}')
        logging.info(f'restart={restart}')
        while self.update(restart) > self.error_magnitude:
            logging.info('not converged, iterating...')

    def test(self,cycles):
        results = []
        for i in range(cycles):
            self.course.driver.set_loc(choice(self.course.start))
            self.course.driver.set_spd([0,0])
            steps = 0
            loc,spd = self.course.driver.loc, self.course.driver.spd
            while self.course.track[loc[0]][loc[1]] != 'F':
                if random() < 0.8:
                    a = choice(self.policy[str(loc)][str(spd)])
                    loc,spd = self.course.project_driver(a_x=a[0],a_y=a[1])
                    self.course.place_driver(loc,spd)
                else:
                    loc,spd = self.course.project_driver(a_x=0,a_y=0)
                    self.course.place_driver(loc,spd)
                steps += 1

            results.append(steps)

        return results

    def test_restart(self,cycles,restart=False):
        results = []
        for i in range(cycles):
            self.course.driver.set_loc(choice(self.course.start))
            self.course.driver.set_spd([0,0])
            steps = 0
            loc,spd = self.course.driver.loc, self.course.driver.spd
            while self.course.track[loc[0]][loc[1]] != 'F':
                if random() < 0.8:
                    a = choice(self.policy[str(loc)][str(spd)])
                    loc,spd = self.course.project_driver(a_x=a[0],a_y=a[1],restart=restart)
                    self.course.place_driver(loc,spd)
                else:
                    loc,spd = self.course.project_driver(a_x=0,a_y=0,restart=restart)
                    self.course.place_driver(loc,spd)
                steps += 1

            results.append(steps)

        return results

if __name__ == "__main__":
    print('-- value iteration --')
    track = sys.argv[1]
    print(f'building from {track}')
    crs = course()
    crs.build_course(track)

    V = ValueIterator(0.85,0.01)
    V.initialize(crs)
    V.run()

    print(f'results of policy (average steps): {np.mean(V.test(100))}')