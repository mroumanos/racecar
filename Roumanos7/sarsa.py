from random import choice, randint, random, uniform
import numpy as np
import logging
import sys
from racetrack import course

class SARSALearner():

    # These will be learned
    TX_PROB = 0.8
    REWARD = -1

    def __init__(self,gamma,epsilon):
        self.gamma = gamma
        self.epsilon = epsilon

    def initialize(self,course):
        self.course = course
        self.Q = course.get_q()

    def get_next_action(self,loc,spd):
        """
        Gets the next action based on a greedy epsilon policy

        Args:
            loc(int array): current location
            spd(int array): current velocity
        """
        if random() < self.epsilon:
            a = choice(list(self.Q[str(loc)][str(spd)].keys()))
            return self.Q[str(loc)][str(spd)][a]['action']
        else:
            return max(self.Q[str(loc)][str(spd)].items(), key=lambda x: x[1]['val'])[1]['action']

    def cycle(self,restart=False):
        """
        Iteratively cycles through decisions and edits Q values according
        to SARSA algorithm until a final state is reached

        Kwargs:
            restart(bool): if crash-restart is needed
        """
        self.course.initialize_driver()
        loc,spd = self.course.driver.loc, self.course.driver.spd
        a = self.get_next_action(loc,spd)
        count = 0
        while self.course.track[loc[0]][loc[1]] != 'F':
            if random() < self.TX_PROB:
                new_loc,new_spd = self.course.project_driver(a_x=a[0],a_y=a[1],restart=restart)
            else:
                new_loc,new_spd = self.course.project_driver(restart=restart)

            new_a = self.get_next_action(new_loc,new_spd)
            new_q = self.Q[str(new_loc)][str(new_spd)][str(new_a)]['val']
            q = self.Q[str(loc)][str(spd)][str(a)]['val']

            alpha = random()

            q_update = q + alpha * (self.REWARD + self.gamma*new_q - q)

            self.Q[str(loc)][str(spd)][str(a)]['val'] = q_update

            self.course.place_driver(new_loc,new_spd)
            loc,spd,a = new_loc,new_spd,new_a
            count += 1


        return count

    def run(self,cycles,restart=False):
        logging.info('performing SARSA learning:')
        logging.info(f'gamma={self.gamma}')
        logging.info(f'epsilon={self.epsilon}')

        iters = []
        for i in range(cycles):
            itr = self.cycle(restart=restart)
            iters.append(itr)

        logging.info(f'complete, average iterations={np.mean(iters)}')

        return iters

    def test(self,cycles,restart=False):

        logging.info(f'testing over {cycles} cycles...')

        counts = []
        for i in range(cycles):
            self.course.initialize_driver()
            loc,spd = self.course.driver.loc, self.course.driver.spd
            count = 0
            pings = 0
            while self.course.track[loc[0]][loc[1]] != 'F':
                a = self.get_next_action(loc,spd)
                if random() < self.TX_PROB:
                    new_loc,new_spd = self.course.project_driver(a_x=a[0],a_y=a[1],restart=restart)
                else:
                    new_loc,new_spd = self.course.project_driver(restart=restart)
                if loc == new_loc and spd == new_spd:
                    pings += 1
                else:
                    pings = 0
                if pings > 100:
                    new_loc,new_spd = self.course.project_driver(a_x=choice([-1,0,1]),a_y=choice([-1,0,1]),restart=restart)

                self.course.place_driver(new_loc,new_spd)

                loc,spd = self.course.driver.loc, self.course.driver.spd
                count += 1

            counts.append(count)

        logging.info(f'testing complete: average steps={np.mean(counts)}')

        return counts

if __name__ == "__main__":
    print('-- SARSA learning --')
    track = sys.argv[1]
    print(f'building from {track}')

    crs = course()
    crs.build_course(track)

    S = SARSALearner(0.85,0.1)
    S.initialize(crs)
    S.run(5000)

    print(f'results of policy (average steps): {np.mean(S.test(100))}')
