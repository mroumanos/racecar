from random import choice, randint, random, uniform
import numpy as np
from math import floor,ceil,exp
import os
import logging

logging.basicConfig(filename='project7.log',level=logging.DEBUG)

class course():
    """
    Describes a racing course containing a racetrack and other attributes.

    Attributes:
        track(list(list([int,int])): 2-D array of an [x,y] coordinate system describing the track
        shape([int,int]): Width and height of the entire course, respectively
        start(list([int,int])): List of starting points
        finish(list([int,int])): List of completion points
        driver(driver): Object containing driver attributes and functions
        havens(dict(dict())): Lookup tables for possible move location given a starting location
        steps(dict()): Lookup table for points traversed during a move -- used for collision evaluation
    """

    def __init__(self):
        self.track = [] # 2-D array of track
        self.shape = [0,0] # shape of the track
        self.start = [] # coordinates for starting line
        self.finish = [] # coordinates for finish line
        self.driver = driver() # single driver object
        self.steps = {} # shows trackmarks of driver between moves

    def build_course(self,file):
        """
        Builds course from a given file. An [x,y] coordinate system
        is used and centers [0,0] on the bottom left of the file text.
        Args:
            file (string): name of file describing a racetrack
        Returns:
            None
        """
        with open(file,'r') as f:
            strips = []

            for i,line in enumerate(f):

                # if it's the first line, get the
                # track information (the shape)
                if i == 0:
                    shape = line.split(',')
                    height = int(shape[0])
                    width = int(shape[1])
                    self.shape = [ width, height ]

                # otherwise, iterate through every
                # character in the line and add it to the
                # appropriate columnar strips
                else:
                    for j,char in enumerate(line):
                        if char == '\n':
                            break
                        try:
                            strips[j]
                        except IndexError:
                            strips.append([])
                        if char == 'S':
                            self.start.append([j,height-i])
                        if char == 'F':
                            self.finish.append([j,height-i])
                        strips[j].append(char)

            # once all columnar strips are collected,
            # provide in reverse order so they can be
            # read in traditional (x,y) fashion
            self.track = []
            for strip in strips:
                self.track.append(list(reversed(strip)))

            self.driver.generate_policy(self.track)
            self.initialize_driver()

            logging.info(f'imported and transformed file {file}')

    def initialize_driver(self):
        """
        Establishes an initial state at the starting line
        """
        self.driver.set_loc(choice(self.start))
        self.driver.set_spd([0,0])


    def place_driver(self,loc,spd):
        """
        Establishes a given state (location and speed) for the driver
        """
        self.driver.set_loc(loc)
        self.driver.set_spd(spd)

    def drive(self,prob=0.8,restart=False):
        """
        Moves the driver by applying policy to current state
        according to probability of success
        """
        loc = self.driver.loc
        spd = self.driver.spd

        if random() < prob:
            a = choice(self.get_actions(loc))
        else:
            a = [0,0]

        new_loc,new_spd = self.project_driver(a[0],a[1],restart=restart)
        self.place_driver(new_loc,new_spd)

        return self.track[new_loc[0]][new_loc[1]]

    def project_driver(self,a_x=0,a_y=0,restart=False):
        """
        Simulates an acceleration to the driver and places driver in
        new location

        Kwargs:
            a_x(int): acceleration to apply to x-axis velocity
            a_y(int): acceleration to apply to y-axis velocity
        """

        # get driver's location after applying acceleration
        old_loc = self.driver.loc
        new_loc,new_spd = self.driver.project(a_x,a_y)

        # determine if the initial and final points connect without crashing
        accident,final_loc = self.connect(old_loc,new_loc)

        if accident:
            new_spd = [0,0]
            if restart:
                final_loc = choice(self.start)

        # return the final state (location and speed)
        return final_loc, new_spd

    def connect(self,old,new):
        """
        This uses a linear approximator that determines all points
        that would be traversed if a driver moves from an old location to
        a new location, and if those points would be obstructions and cause
        a crash (wall or track boundary). If a crash would occur, a
        new location on the track closest to the point of impact is returned.
        If the vehicle does traverse a finish line element, the vehicle
        stops on the finish line (regardless of velocity) but no collision occurs.

        Args:
            old([int,int]): [x,y] coordinates of starting location
            new([int,int]): [x,y] coordinates of end location

        Return:
            bool: True if a crash would occur; False, otherwise.
            [int,int]: final location whether collision occurred or not
        """

        if old == new:
            return False,old

        x_i,y_i = old[0],old[1]
        x_f,y_f = new[0],new[1]

        # make a straight-line from the old
        # coordinates to the new and ensure
        # the driver did not tunnel through
        # any walls or boundaries
        d_x = x_f - x_i
        d_x_sign = -1 if d_x < 0 else 1
        d_y = y_f - y_i
        d_y_sign = -1 if d_y < 0 else 1

        m_x, m_y = 0,0
        if abs(d_x) > abs(d_y):
            m_x = d_x_sign
            m_y = abs(d_y/d_x) * d_y_sign
            adj = lambda x,y: [ [int(x),ceil(y)], [int(x),floor(y)] ] if y%1 else [ [int(x),int(y)] ]

        else:
            m_x = abs(d_x/d_y) * d_x_sign
            m_y = d_y_sign
            adj = lambda x,y: [ [ceil(x),int(y)], [floor(x),int(y)] ] if x%1 else [ [int(x),int(y)] ]

        self.steps = {}
        last = old
        while [ int(x_i), int(y_i) ] != [ x_f, y_f ]:
            x_i = x_i + m_x
            y_i = y_i + m_y

            adj_steps = adj(x_i,y_i)
            for a in adj_steps:
                if not self.in_bounds(a):
                    return True,last
                elif self.track[a[0]][a[1]] == 'F': # you've passed through the finish line!
                    return True,a # set to True to create one simple state on the finish line
                else:
                    self.steps[str(a)] = a
                    last = a

        return False,new

    def in_bounds(self,loc):
        """
        Determines if coordinates are on the legal track (inclusive
        of start and finish lines)

        Args:
            loc([int,int]): [x,y] coordinates being evaluated
        """
        x = loc[0]
        y = loc[1]

        if x < 0 or x > self.shape[0]-1:
            return False
        elif y < 0 or y > self.shape[0]-1:
            return False
        elif self.track[x][y] == '#':
            return False
        else:
            return True

    def simulate(self,cycles=1):
        """
        Simulates driver attempting to reach finish line
        according to current policy
        """
        logging.info(f'simulating random walk over {cycles} cycles')
        results = []
        for i in range(cycles):
            self.driver.set_loc(choice(self.start))
            self.driver.set_spd([0,0])
            steps = 0
            while self.drive() != 'F':
                steps += 1
            results.append(steps)

        logging.info(f'average steps per race: {np.mean(results)}')

        return results

    def simulate_restart(self,cycles=1):
        """
        Simulates driver attempting to reach finish line
        using a crash_restart implementation
        """
        logging.info(f'simulating random walk over {cycles} cycles with crash_restart method')
        results = []
        for i in range(cycles):
            self.driver.set_loc(choice(self.start))
            self.driver.set_spd([0,0])
            steps = 0
            while self.drive(restart=True) != 'F':
                steps += 1
                if steps > 100000:
                    break
            results.append(steps)

        logging.info(f'average steps per race: {np.mean(results)}')

        return results

    def simulate_visual(self):
        """
        Same as simulate() except provides visual of driver
        and track. Each move gets progressively faster with
        total number of steps taken.
        """
        self.driver.set_loc(choice(self.start))
        self.driver.set_spd([0,0])
        steps = 0
        print(f'simulation step: {steps}')
        self.print_track()
        while self.drive() != 'F':
            steps += 1
            os.system('clear')
            print(f'simulation step: {steps}')
            self.print_track()
            time.sleep(1/steps)
        print(f'simulation complete at step: {steps}')

    def print_track(self):
        """
        Prints the track and identifies driver with
        its chosen icon
        """
        track = ''
        for i in range(self.shape[1]):
            for j in range(self.shape[0]):
                x = j
                y = self.shape[1]-i-1
                if self.driver.loc == [x,y]:
                    track += self.driver.icon
                else:
                    track += self.track[x][y]
            track += '\n'
        print(track)

    def get_states(self):
        """
        Gets states and establishes a baseline of necessary
        attributes for each (value,actions,rewards,transition probabilities)
        """
        states = {}

        v_min, v_max = self.driver.V_MIN, self.driver.V_MAX
        for x,i in enumerate(self.track):
            for y,j in enumerate(i):
                if self.track[x][y] != '#':
                    states[str([x,y])] = {}

                    if self.track[x][y] == 'F':
                        states[str([x,y])][str([0,0])] = {} # the only state permitted on finish line
                        states[str([x,y])][str([0,0])]['loc'] = [x,y]
                        states[str([x,y])][str([0,0])]['spd'] = [0,0]
                        states[str([x,y])][str([0,0])]['val'] = 0
                        states[str([x,y])][str([0,0])]['actions'] = []
                    else:
                        for x_i in np.mgrid[v_min:v_max+1]:
                            for y_i in np.mgrid[v_min:v_max+1]:
                                states[str([x,y])][str([x_i,y_i])] = {}
                                states[str([x,y])][str([x_i,y_i])]['loc'] = [x,y]
                                states[str([x,y])][str([x_i,y_i])]['spd'] = [x_i,y_i]
                                states[str([x,y])][str([x_i,y_i])]['val'] = 0
                                states[str([x,y])][str([x_i,y_i])]['actions'] = self.get_actions([x,y])
                                states[str([x,y])][str([x_i,y_i])]['reward'] = -1
                                states[str([x,y])][str([x_i,y_i])]['tx'] = [1,0]
                                states[str([x,y])][str([x_i,y_i])]['pr'] = [0.8,0.2]
        return states

    def get_q(self):
        """
        Gets Q values and establishes a baseline of necessary
        attributes for each (value,learning rate)
        """
        q = {}

        v_min, v_max = self.driver.V_MIN, self.driver.V_MAX
        for x,i in enumerate(self.track):
            for y,j in enumerate(i):
                if self.track[x][y] != '#':
                    q[str([x,y])] = {}
                    for x_i in np.mgrid[v_min:v_max+1]:
                        for y_i in np.mgrid[v_min:v_max+1]:
                            q[str([x,y])][str([x_i,y_i])] = {}
                            for a in self.get_actions([x,y]):
                                q[str([x,y])][str([x_i,y_i])][str(a)] = {}
                                q[str([x,y])][str([x_i,y_i])][str(a)]['action'] = a
                                q[str([x,y])][str([x_i,y_i])][str(a)]['val'] = uniform(-1,1)
                                q[str([x,y])][str([x_i,y_i])][str(a)]['alpha'] = random()
        return q

    def get_actions(self,loc):
        """
        Generates available actions based on driver's
        acceleration limits
        """
        x = loc[0]
        y = loc[1]

        actions = []
        if self.track[x][y] == '.' or 'S':
            actions = []
            a_min, a_max = self.driver.A_MIN, self.driver.A_MAX
            for dx in np.mgrid[a_min:a_max+1]:
                for dy in np.mgrid[a_min:a_max+1]:
                    actions.append([dx,dy])

        return actions

class driver():

    V_MIN, V_MAX = -5,5
    A_MIN, A_MAX = -1,1
    P_DIST = [0.8,0.2]

    def __init__(self):
        self.loc = [0,0]
        self.spd = [0,0]
        self.icon = 'X'


    def generate_policy(self,track):
        """
        Creates a blank policy with available actions (state-action pairs)

        Args:
            track(2-d array): describes the track so state-action pairs can be made
        """
        self.policy = {}
        for x,i in enumerate(track):
            for y,j in enumerate(i):
                if track[x][y] != '#':
                    self.policy[str([x,y])] = {}
                    for x_i in np.mgrid[self.V_MIN:self.V_MAX+1]:
                        for y_i in np.mgrid[self.V_MIN:self.V_MAX+1]:
                            self.policy[str([x,y])][str([x_i,y_i])] = []
                            for dx in np.mgrid[self.A_MIN:self.A_MAX+1]:
                                for dy in np.mgrid[self.A_MIN:self.A_MAX+1]:
                                    self.policy[str([x,y])][str([x_i,y_i])].append([dx,dy])

    def accelerate(self,a_x,a_y):
        """
        Creates acceleration to the vehicle on both the x and y axis.

        Args:
            a_x(int): x-axis acceleration, within {-1,0,+1}
            a_y(int): y-axis acceleration, within {-1,0,+1}
        """

        # employs random failure of acceleration according to static P_DIST
        a_x = np.random.choice([a_x,0],p=self.P_DIST)
        a_y = np.random.choice([a_y,0],p=self.P_DIST)

        a_x = np.clip(a_x,self.A_MIN,self.A_MAX)
        v_x_i = self.spd[0]
        v_x_f = np.clip(self.spd[0] + a_x,self.V_MIN,self.V_MAX)

        a_y = np.clip(a_y,self.A_MIN,self.A_MAX)
        v_y_i = self.spd[1]
        v_y_f = np.clip(self.spd[1] + a_y,self.V_MIN,self.V_MAX)

        self.spd = [ v_x_f, v_y_f ]


    def project(self,a_x,a_y):
        """
        Simulates acceleration to the vehicle on both the x and y axis.

        Args:
            a_x(int): x-axis acceleration, within {-1,0,+1}
            a_y(int): y-axis acceleration, within {-1,0,+1}
        """

        a_x = np.clip(a_x,self.A_MIN,self.A_MAX)
        v_x_i = self.spd[0]
        v_x_f = np.clip(self.spd[0] + a_x,self.V_MIN,self.V_MAX)

        a_y = np.clip(a_y,self.A_MIN,self.A_MAX)
        v_y_i = self.spd[1]
        v_y_f = np.clip(self.spd[1] + a_y,self.V_MIN,self.V_MAX)

        x_new = self.loc[0] + v_x_f
        y_new = self.loc[1] + v_y_f

        return [ x_new, y_new ], [ v_x_f, v_y_f ]

    def set_loc(self,loc):
        self.loc = loc

    def set_spd(self,spd):
        self.spd = spd