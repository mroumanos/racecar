import logging
import numpy as np
import sys

from Roumanos7.racetrack import course
from Roumanos7.sarsa import SARSALearner


class QLearner(SARSALearner):
    def __init__(self,gamma):
        self.gamma = gamma

    def get_next_action(self,loc,spd):
        # maximum
        return max(self.Q[str(loc)][str(spd)].items(), key=lambda x: x[1]['val'])[1]['action']

    def run(self,cycles,restart=False):
        logging.info('performing Q-learning:')
        logging.info(f'gamma={self.gamma}')

        iters = []
        for i in range(cycles):
            iters.append(self.cycle(restart=restart))

        logging.info(f'complete, average iterations={np.mean(iters)}')

        return iters

if __name__ == "__main__":
    print('-- Q-Learning --')
    track = sys.argv[1]
    print(f'building from {track}')

    crs = course()
    crs.build_course(track)

    Q = QLearner(0.85)
    Q.initialize(crs)
    Q.run(5000)

    print(f'results of policy (average steps): {np.mean(Q.test(100))}')